# document

이전에 들었던 교육: 빅데이터(딥러닝) 활용 AI 설계

|               |                           |      |
| :------------ | :------------------------ | :--- |
| 인터페이스    | HTML                      | 2    |
| 개발          | CSS                       | 2    |
| 프로젝트      | JavaScript                | 2    |
|               | SQL                       | 2    |
|               | NoSql                     | 2    |
|               | Python                    | 3    |
| 클라우드      | Cloud 개념                | 2    |
| (Fundamental, | Django                    | 2    |
| 인프라,       | Linux                     | 2    |
| 하이브리드)   | Network                   | 2    |
|               | Docker                    | 2    |
|               | Docker  Compose           | 2    |
|               | Kubernetes                | 2    |
|               | OpenStack                 | 2    |
|               | AWS                       | 2    |
| MSA           | MSA개념                   | 1    |
|               | DDD(Domain Driven Design) | 1    |
|               | Serverless                | 2    |
|               | Backend Rest API          | 2    |
|               | Front Axios(Promise)      | 2    |
